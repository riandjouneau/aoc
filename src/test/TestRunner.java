package test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(TestJunit.class);
      int count = 1;
      for (Failure failure : result.getFailures()) {
         System.err.println("Error "+count+" on "+result.getFailureCount()+" : "+failure.toString());
         count++;
      }
      System.err.println("Final success on "+result.getRunCount()+" tests : "+result.wasSuccessful());
   }
}  	