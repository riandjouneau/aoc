package test;

import org.junit.Test;

import controller.ControllerImpl;
import controller.interfaces.Controller;
import junit.framework.TestCase;
import model.Engine;
import model.EngineImpl;

import static org.junit.Assert.assertEquals;

public class TestJunit extends TestCase {
	
	private ControllerImpl controller;
	private Engine engine;

	protected void setUp(){
		engine = new EngineImpl();
		controller = new ControllerImpl(engine);
	}

	@Test
	public void testIncBasic() {
		int before = engine.getMeasure();
		controller.inc();
		int theory = before+1;
		int after= engine.getMeasure();
		assertEquals(theory,after);
	}

	@Test
	public void testIncMax() {
		for(int i = 0;i<30;i++) controller.inc();
		int after= engine.getMeasure();
		assertTrue(after<=controller.MAX_MEASURE);
	}

	@Test
	public void testDecBasic() {
		int before = engine.getMeasure();
		controller.dec();
		int theory = before-1;
		int after= engine.getMeasure();
		assertEquals(theory,after);
	}

	@Test
	public void testDecMin() {
		for(int i = 0;i<30;i++) controller.dec();
		int after= engine.getMeasure();
		assertTrue(after>=controller.MIN_MEASURE);
	}
}