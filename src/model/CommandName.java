package model;

public enum CommandName{
	UPDATE_TEMPO, 
	UPDATE_RUNNING,
	MARK_TEMPO,
	MARK_MEASURE
}
