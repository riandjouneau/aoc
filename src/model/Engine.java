package model;

import controller.interfaces.Command;

public interface Engine {
	/**
	 * Getter for tempo
	 * @return
	 */
	public int getTempo();
	
	/**
	 * Setter for tempo
	 * @param t
	 */
	public void setTempo(int t);
	
	/**
	 * Getter for tempo
	 * @return
	 */
	public int getMeasure();
	
	/**
	 * Setter for measure
	 * @param t
	 */
	public void setMeasure(int t);
	
	/**
	 * Getter for running
	 * @return
	 */
	public boolean getRunning();
	
	/**
	 * Setter for Running
	 * @param b
	 */
	public void setRunning(boolean b);
	
	/**
	 * Adds a command with its name to an HashMap of commands
	 * @param name
	 * @param c
	 */
	public void setCmd(CommandName name, Command c);
}
