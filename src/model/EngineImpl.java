package model;

import java.util.HashMap;

import controller.ClockImpl;
import controller.interfaces.Clock;
import controller.interfaces.Command;

public class EngineImpl implements Engine{
	
	private int tempo;
	private int measure;
	private boolean running;
	private HashMap<CommandName,Command> commands;
	private Command tic;
	private Clock clock;
	private int ticCounter;
	
	public EngineImpl() {
		commands = new HashMap<CommandName,Command>();
		tempo = 60;
		measure = 4;
		running = false;
		clock = new ClockImpl();
		tic = () -> this.tic();
	}

	@Override
	public int getTempo() {
		return tempo;
	}

	@Override
	public void setTempo(int t) {
		tempo = t;
		commands.get(CommandName.UPDATE_TEMPO).execute();
		if(running){
			clock.deactivate(tic);
			clock.activatePeriodically(tic, getPeriodInSeconds());
		}
	}
	
	private int getPeriodInSeconds(){
		return (int) (60.0/this.getTempo() *1000);
	}

	@Override
	public int getMeasure() {
		return measure;
	}

	@Override
	public void setMeasure(int m) {
		measure = m;
	}

	@Override
	public boolean getRunning() {
		return running;
	}

	@Override
	public void setRunning(boolean b) {
		running = b;
		if(running){
			clock.activatePeriodically(tic, getPeriodInSeconds());
		}else{
			clock.deactivate(tic);
		}
		commands.get(CommandName.UPDATE_RUNNING).execute();
	}

	@Override
	public void setCmd(CommandName name, Command c) {
		commands.put(name, c);
	}
	
	private void tic(){
		ticCounter = (ticCounter+1)%measure;
		
		if(ticCounter==0)
			commands.get(CommandName.MARK_MEASURE).execute();
		else
			commands.get(CommandName.MARK_TEMPO).execute();
	}
}
