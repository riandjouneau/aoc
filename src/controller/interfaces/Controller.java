package controller.interfaces;

public interface Controller {
	/**
	 * Marks a tempo (sound+light)
	 */
	public void markTempo();
	
	/**
	 * Marks a measure (sound+light)
	 */
	public void markMeasure();
	
	/**
	 * Updates the wheel value from the wheel and gives it to the Engine
	 */
	public void updateWheel();
	
	/**
	 * Updates the tempo value in the display
	 */
	public void updateTempo();
	
	/**
	 * Starts the metronome, if not already running
	 */
	public void start();
	
	/**
	 * Stops the metronome, if not already stopped
	 */
	public void stop();
	
	/**
	 * Says if the metronome is currently running or not
	 */
	public void updateRunning();
	
	/**
	 * Increases the measure (min: MIN_MEASURE, max: MAX_MEASURE)
	 */
	public void inc();
	
	/**
	 * Decreases the measure (min: MIN_MEASURE, max: MAX_MEASURE)
	 */
	public void dec();
}
