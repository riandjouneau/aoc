package controller.interfaces;

public interface Clock {
	/**
	 * Calls the command every "periodInSeconds" seconds
	 * @param cmd
	 * @param periodInSeconds
	 */
	public void activatePeriodically(Command cmd, float periodInSeconds);
	
	/**
	 * Calls the command once, after "delayInSeconds" seconds
	 * @param cmd
	 * @param delayInSeconds
	 */
	public void acivateAfterDelay(Command cmd, float delayInSeconds);
	
	/**
	 * Removes the command from the list of command to call
	 * @param cmd
	 */
	public void deactivate(Command cmd);
}
