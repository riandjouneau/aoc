package controller;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import controller.interfaces.Clock;
import controller.interfaces.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.CommandName;
import model.Engine;
import model.EngineImpl;

public class ControllerImpl implements Controller{
	
	private final Color LED_ON = Color.rgb(255,0,0);
	private final Color LED_OFF = Color.rgb(130,0,0);
	public final int MIN_MEASURE = 2;
	public final int MAX_MEASURE = 7;
	private URL tempoURL;
	private URL measureURL;
	
	private Engine engine;
	
	@FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button incButton;

    @FXML
    private Circle led1;

    @FXML
    private Button startButton;

    @FXML
    private Circle led2;

    @FXML
    private Slider wheel;

    @FXML
    private Button decButton;

    @FXML
    private Button stopButton;

    @FXML
    private Label display;
    
    private AudioClip tempoClip;
    private AudioClip measureClip;
	
	public ControllerImpl() {
		engine = new EngineImpl();
		init();
	}
	
	public ControllerImpl(Engine _engine) {
		engine = _engine;
		init();
	}
	
	private void init(){
		engine.setCmd(CommandName.UPDATE_TEMPO, () -> this.updateTempo());
		engine.setCmd(CommandName.UPDATE_RUNNING, () -> this.updateRunning());
		engine.setCmd(CommandName.MARK_TEMPO, () -> this.markTempo());
		engine.setCmd(CommandName.MARK_MEASURE, () -> this.markMeasure());
		tempoURL = getClass().getResource("/resources/tempoClip_1.wav");
		tempoClip = new AudioClip(tempoURL.toString());
		measureURL = getClass().getResource("/resources/measureClip_1.wav");
		measureClip = new AudioClip(measureURL.toString());
	}

	@Override
	public void markTempo() {
		tempoClip.play();
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run(){
				led1.setFill(LED_OFF);
			}
		}, 100);
		led1.setFill(LED_ON);
	}

	@Override
	public void markMeasure() {
		measureClip.play();
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run(){
				led2.setFill(LED_OFF);
			}
		}, 100);
		led2.setFill(LED_ON);
	}

	@Override
	public void updateWheel() {
		int value = (int) wheel.getValue();
		engine.setTempo(value);
	}

	@Override
	public void updateTempo() {
		display.setText(""+engine.getTempo());
	}
	
	@Override
	public void updateRunning() {
		System.out.println((engine.getRunning())?"started":"stopped");
	}
	
	@Override
	@FXML
	public void start() {
		if(!engine.getRunning()){
			engine.setRunning(true);
		}
	}

	@Override
    @FXML
    public void stop() {
		if(engine.getRunning()){
			engine.setRunning(false);
		}
    }

	@Override
    @FXML
    public void inc() {
		int m = engine.getMeasure()+1;
		if(m >= MIN_MEASURE && m <= MAX_MEASURE){
			engine.setMeasure(m);
		}
    }

	@Override
    @FXML
    public void dec() {
		int m = engine.getMeasure()-1;
		if(m >= MIN_MEASURE && m <= MAX_MEASURE){
			engine.setMeasure(m);
		}
    }

    @FXML
    void initialize() {
        assert incButton != null : "fx:id=\"incButton\" was not injected: check your FXML file 'metronome.fxml'.";
        assert led1 != null : "fx:id=\"led1\" was not injected: check your FXML file 'metronome.fxml'.";
        assert startButton != null : "fx:id=\"startButton\" was not injected: check your FXML file 'metronome.fxml'.";
        assert led2 != null : "fx:id=\"led2\" was not injected: check your FXML file 'metronome.fxml'.";
        assert wheel != null : "fx:id=\"wheel\" was not injected: check your FXML file 'metronome.fxml'.";
        assert decButton != null : "fx:id=\"decButton\" was not injected: check your FXML file 'metronome.fxml'.";
        assert stopButton != null : "fx:id=\"stopButton\" was not injected: check your FXML file 'metronome.fxml'.";
        assert display != null : "fx:id=\"display\" was not injected: check your FXML file 'metronome.fxml'.";
        
        led1.setFill(LED_OFF);
        led2.setFill(LED_OFF);
        
        wheel.valueProperty().addListener(new ChangeListener<Number>() {
        	@Override
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    updateWheel();
            }
        });
    }
}
