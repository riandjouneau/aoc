package controller;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import controller.interfaces.Clock;
import controller.interfaces.Command;
import model.CommandName;

public class ClockImpl implements Clock {
	
	private HashMap<Command,Timer> commands;
	
	public ClockImpl() {
		commands = new HashMap<Command,Timer>();
	}

	@Override
	public void activatePeriodically(Command cmd, float periodInSeconds) {
		if(commands.containsKey(cmd)){
			deactivate(cmd);
		}
		Timer timer = new Timer();
		TimerTask timerTask = new TimerTask(){
			@Override
			public void run(){
				cmd.execute();
			}
		};
		timer.scheduleAtFixedRate(timerTask, 500, (long) periodInSeconds);
		commands.put(cmd, timer);
	}

	@Override
	public void acivateAfterDelay(Command cmd, float delayInSeconds) {
		if(commands.get(cmd) != null){
			deactivate(cmd);
		}
		Timer timer = new Timer();
		TimerTask timerTask = new TimerTask(){
			@Override
			public void run(){
				cmd.execute();
			}
		};
		timer.schedule(timerTask, (long) delayInSeconds);
		commands.put(cmd, timer);
	}

	@Override
	public void deactivate(Command cmd) {
		commands.get(cmd).cancel();
		commands.remove(cmd);
	}

}
